%if 0%{?fedora} >= 12
%define create_debuginfo_subpackage 1
%else
%define create_debuginfo_subpackage 0
%endif

%define __strip %{_mingw32_strip}
%define __objdump %{_mingw32_objdump}
%define _use_internal_dependency_generator 0
%define __find_requires %{_mingw32_findrequires}
%define __find_provides %{_mingw32_findprovides}

%if %{create_debuginfo_subpackage}
%define __debug_install_post %{_mingw32_debug_install_post}
%endif


# The number N in libexif-N.dll
%global libexif_major 12


Name:		mingw32-libexif
Version:	0.6.19
Release:	1%{?dist}
Summary:	Library for extracting extra information from image files

Group:		System Environment/Libraries
License:	LGPLv2+
URL:		http://libexif.sourceforge.net/
Source0:	http://prdownloads.sourceforge.net/libexif/libexif-%{version}.tar.bz2
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{create_debuginfo_subpackage}
BuildRequires:	mingw32-filesystem >= 53
%else
BuildRequires:	mingw32-filesystem >= 50
%endif
BuildRequires:	mingw32-binutils
BuildRequires:	mingw32-gcc
BuildRequires:	pkgconfig

BuildArch:	noarch


%if %{create_debuginfo_subpackage}
# Use a ? to expand to nothing if undefined, enabling F13 mock builds on F11.
# This avoids the trick of hiding the macro in %%description which in turn
# confuses the koji webinterface.
%{?_mingw32_debug_package}
%endif


%description
Most digital cameras produce EXIF files, which are JPEG files with
extra tags that contain information about the image. The EXIF library
allows you to parse an EXIF file and read the data from those tags.


%prep
%setup -n libexif-%{version} -q


%build
%{_mingw32_configure} --disable-static --enable-shared --disable-docs --disable-nls
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mv $RPM_BUILD_ROOT%{_mingw32_docdir}/libexif docdir


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README NEWS
%{_mingw32_bindir}/libexif-%{libexif_major}.dll
%{_mingw32_includedir}/libexif
%{_mingw32_libdir}/libexif.dll.a
%{_mingw32_libdir}/libexif.la
%{_mingw32_libdir}/pkgconfig/libexif.pc


%changelog
* Sat Jan  2 2010 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.6.19-1
- Initial mingw32-libexif package
